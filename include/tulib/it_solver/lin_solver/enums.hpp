#ifndef TULIB_LIN_SOLVER_ENUMS_INCLUDED
#define TULIB_LIN_SOLVER_ENUMS_INCLUDED

#include <tulib/util/input_definitions.hpp>

namespace tulib::lin_solver
{

/**
 * Type of preconditioner.
 *  - NONE: No preconditioner.
 *  - USER_DEFINED: Defined by user in the functor class.
 **/
enum class preconType : int
{  NONE = 0
,  USER_DEFINED         // Left preconditioning with user-defined matrix
,  USER_DEFINED_RIGHT   // Right preconditioning with user-defined matrix
};
bool is_right_preconditioner
   (  preconType precon_
   )
{
   return precon_ == preconType::USER_DEFINED_RIGHT;
}

/**
 * Type of solver
 *  - CG: Conjugate Gradient
 *  - CR: Conjugate Residual
 *  - GMRES: Generalized Minimum RESidual
 **/
enum class solverType : int
{  CG = 0
,  CR
,  GMRES
};

/**
 * Type of convergence checks
 *  - RESIDUALNORM: L2 Norm of residual vector
 *  - RELATIVERESNORM: L2 Norm of residual vector divided by norm of RHS.
 *  - RESIDUALMAXNORM: Inf. norm of residual vector (i.e. largest element)
 **/
enum class convType : int
{  RESIDUALNORM = 0
,  RELATIVERESNORM
,  RESIDUALMAXNORM
};

/**
 * Type of initialization (x0)
 * - ZERO: x0 = 0
 * - RHS: x0 = b (for Ax=b)
 * - USERDEFINDED: Use the user-provided initial_guess method (if it exists)
 **/
enum class initType : int
{  ZERO = 0
,  RHS
,  USERDEFINED
};

/**
 * Possible input arguments to linear solver
 **/
enum class inputKey : int
{  SOLVER = 0        //< Solver type
,  PRECONDITIONER    //< Type of preconditioner
,  MAXITER           //< Maximum number of iterations
,  THRESHOLD         //< Convergence threshold
,  CONVCHECK         //< Type of convergence check
,  PRCG              //< Use the Polak-Ribiere update formula instead of Fletcher-Reeves in conjugate gradient.
,  OUTPUT            //< tulib::io::writer_collection used for outputting solver info
,  RESTARTPREFIX     //< Prefix for restart files.
,  ALLOWRESTART      //< Allow the algorithm to flush and restart.
,  INITIALGUESS      //< Which type of initial guess to use
};

using settings_type = util::input_definitions<inputKey>;

} /* namespace tulib::lin_solver */

#endif /* TULIB_LIN_SOLVER_ENUMS_INCLUDED */