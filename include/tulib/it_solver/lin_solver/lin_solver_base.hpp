#ifndef TULIB_LIN_SOLVER_LIN_SOLVER_BASE_HPP_INCLUDED
#define TULIB_LIN_SOLVER_LIN_SOLVER_BASE_HPP_INCLUDED

#include <vector>
#include <deque>
#include <string_view>
#include <algorithm>
#include <tuple>
#ifdef TULIB_PAR_STL
#include <execution>
#endif
#include <tulib/it_solver/lin_solver/system_wrapper.hpp>
#include <tulib/it_solver/lin_solver/enums.hpp>
#include <tulib/math/container_math_wrappers.hpp>
#include <tulib/util/input_definitions.hpp>
#include <tulib/util/io.hpp>
#include <tulib/util/exceptions.hpp>
#include <tulib/settings.hpp>

namespace tulib::lin_solver
{

/**
 * Base class for linear and eigenvalue solvers
 **/
template
   <  typename F
   >
class lin_solver_base
   :  protected detail::system_wrapper<F>
{
public:
   /** @name Aliases **/
   //!@{
   using vec_type = typename detail::system_wrapper<F>::vec_t;
   using size_type = long;
   using param_type = decltype(math::wrap_dot_product(std::declval<vec_type>(), std::declval<vec_type>()));
   using real_type = meta::real_type_t<param_type>;
   using rhs_type = vec_type;
   using writers_type = io::writer_collection;
   //!@}

   //! C-tor
   lin_solver_base
      (  F&& f_
      ,  const settings_type& settings_
      )
      :  detail::system_wrapper<F>(std::forward<F>(f_))
      ,  _writers(settings_.template get<writers_type>(inputKey::OUTPUT))
      ,  _maxiter(settings_.template get<settings_type::integer_type>(inputKey::MAXITER))
      ,  _convcheck(settings_.template get<convType>(inputKey::CONVCHECK))
      ,  _threshold(settings_.template get<settings_type::float_type>(inputKey::THRESHOLD))
      ,  _precon(settings_.template get<preconType>(inputKey::PRECONDITIONER))
      ,  _restart_prefix(settings_.template get<std::string>(inputKey::RESTARTPREFIX))
      ,  _init_type(settings_.template get<initType>(inputKey::INITIALGUESS))
   {
   }

   //! Virtual d-tor
   virtual ~lin_solver_base() = default;

   /**
    * Prepare solver.
    **/
   void prepare_solver
      (  const rhs_type& rhs_
      )
   {
      io::SCOPE_IOLEVEL(this->_writers, 2);

      if ( this->_prepared )
      {
         TULIB_EXCEPTION("Clear solver before re-running prepare_solver.");
      }

      // Print info
      this->_writers << "######################################################" << "\n"
                     << "# Start solving linear equations                     #" << "\n"
                     << "######################################################" << "\n"
                     << "   - Solver type:   " << this->type() << "\n"
                     << "   - Equation type: " << typeid(F).name() << "\n"
                     << "   - Vector type:   " << typeid(vec_type).name() << "\n"
                     << std::flush;

      // TODO: Look for restart files
      // this->attempt_restart();

      this->_prepared = true;
      this->_rhs = std::cref(rhs_);
      this->_rhs_norm = math::wrap_norm(this->_rhs->get());
      this->prepare_solver_impl(rhs_);
   }

   /**
    * Flush cache and prepare for restarting.
    **/
   void reset()
   {
      this->reset_impl();
   }

   /**
    * Clear solver completely - i.e. prepare for solving a new set of equations.
    **/
   void clear()
   {
      this->_prepared = false;
      this->_rhs = std::nullopt;
      this->clear_impl();
   }

   /**
    * Run solver starting from a given iteration number (called by linear_equation_solver).
    * @param rhs_
    * @param iter_begin_
    * @return
    *    pair(converged?, done, total iteration count)
    **/
   std::tuple<bool, bool, size_type> run_solver
      (  size_type iter_begin_
      )
   {
      io::SCOPE_IOLEVEL(this->_writers, 2);

      // Check if prepare_solver has been called
      if (  !this->_prepared )
      {
         TULIB_EXCEPTION("prepare_solver must be called before starting iterations.");
      }

      // Loop starting from iter_begin
      for(size_type iter = iter_begin_; iter<this->_maxiter; ++iter)
      {
         // Take one iteration step
         bool converged = this->run_iteration(iter);

         // Check convergence
         if ( converged )
         {
            io::SCOPE_INDENT(this->_writers);
            this->_writers << this->type() << " solver converged in " << iter+1 << " iterations." << std::endl;
            return std::make_tuple(true, true, iter);
         }
         // Determine if we shall restart (and we have iterations left)
         else if  (  this->flush_and_restart()
                  )
         {
            io::SCOPE_IOLEVEL(this->_writers, 3);
            this->_writers << "Restart requested!" << std::endl;
            return std::make_tuple(false, false, iter);
         }
      }

      // Not converged, but done.
      return std::make_tuple(false, true, this->_maxiter);
   }

   //! Type
   std::string_view type() const noexcept
   {
      return this->type_impl();
   }

   //! Get the current best solution vector
   vec_type solution() const
   {
      return this->solution_impl();
   }

protected:
   //! Writers
   writers_type _writers;

   /** @name Settings **/
   //!@{
   //! Max iterations
   size_type _maxiter = 50;

   //! Convergence check type
   convType _convcheck = convType::RESIDUALNORM;

   //! Convergence threshold
   real_type _threshold = 1.e-6;

   //! Prefix for restart files
   std::string _restart_prefix;

   //! Type of initial guess
   initType _init_type = initType::ZERO;
   //!@}

   /** @name State **/
   //!@{
   //! Is the solver ready?
   bool _prepared = false;

   //! Const reference to RHS
   optcref<rhs_type> _rhs = std::nullopt;

   //! RHS norm(s)
   real_type _rhs_norm;
   //!@}

   //! Use preconditioning?
   bool use_precon() const
   {
      return this->has_precondition && (this->_precon != preconType::NONE);
   }

   //! Check convergence
   bool check_convergence_impl(real_type rnorm2_) const
   {
      switch( this->_convcheck )
      {
         case convType::RESIDUALNORM:
            return std::sqrt(rnorm2_) < this->_threshold;
         case convType::RELATIVERESNORM:
            return std::sqrt(rnorm2_) / this->_rhs_norm < this->_threshold;
         default:
            TULIB_EXCEPTION("Not implemented for this convergence-check type!");
      }
   }

   //! Run a single iteration. Return true if converged.
   bool run_iteration
      (  size_type iter_
      )
   {
      this->_writers << " ### Iteration " << iter_ << std::endl;
      return this->run_iteration_impl(iter_);
   }

   //! Do we flush and restart?
   bool flush_and_restart() const
   {
      return this->flush_and_restart_impl();
   }

   //! Get initial guess vector(s)
   vec_type initial_guess
      (  const rhs_type& rhs_
      )  const
   {
      switch ( this->_init_type )
      {
         case lin_solver::initType::USERDEFINED:
         {
            if constexpr ( this->has_initial_guess )
            {
               return this->get().initial_guess(rhs_);
            }
            // Else, fall through to ZERO
         }
         case lin_solver::initType::ZERO:
         {
            auto x = rhs_;
            tulib::math::wrap_scale(x, real_type(0.0));
            return x;
         }
         case lin_solver::initType::RHS:
         {
            return rhs_;
         }
         default:
         {
            TULIB_EXCEPTION("Unknown initial-guess type.");
         }
      }
   }

   //! Precondition a vector
   void precondition
      (  vec_type& v_
      )  const
   {
      switch ( this->_precon )
      {
         case preconType::NONE:
            break;
         case preconType::USER_DEFINED:
         {
            if constexpr ( this->has_precondition )
            {
               this->get().precondition(v_);
            }
            break;
         }
         default:
            TULIB_EXCEPTION("Non-implemented preconType");
      }
   }


private:
   //! Type of preconditioner
   preconType _precon = preconType::USER_DEFINED;

   //! Type of solver
   virtual std::string_view type_impl() const = 0;

   //! Clear
   virtual void clear_impl() = 0;

   //! Reset
   virtual void reset_impl(bool first_=false) = 0;

   //! Prepare iteration
   virtual void prepare_solver_impl(const rhs_type& rhs_) = 0;

   //! Run an iteration
   virtual bool run_iteration_impl(size_type iter_) = 0;

   //! Do we flush and restart?
   virtual bool flush_and_restart_impl() const
   {
      return false;
   }

   //! Get current best solution
   virtual vec_type solution_impl() const = 0;
};

} /* namespace tulib::lin_solver */

#endif /* TULIB_LIN_SOLVER_LIN_SOLVER_BASE_HPP_INCLUDED */
