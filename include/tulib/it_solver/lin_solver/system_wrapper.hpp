#ifndef TULIB_LIN_SOLVER_SYSTEM_WRAPPER_HPP_INCLUDED
#define TULIB_LIN_SOLVER_SYSTEM_WRAPPER_HPP_INCLUDED

#include <tulib/util/function_wrapper.hpp>
#include <tulib/meta/is_detected.hpp>

namespace tulib::lin_solver::detail
{
   /**
    * 
    **/
   template
      <  typename F
      >
   class system_wrapper
      :  protected ::tulib::util::function_wrapper<F>
   {
      public:
         //! Alias'
         using base = ::tulib::util::function_wrapper<F>;
         using vec_t = typename base::return_t;
         using arg_t = typename std::tuple_element_t<0, typename base::arg_ts>;

         //! Checks
         static_assert(base::argc == 1);  //< Only _one_ argument
         static_assert(std::is_convertible_v<vec_t, std::remove_const_t<std::remove_reference_t<arg_t>>>); //< Return type must be convertible to argument type

         /** @name Indicators of whether optional methods are available **/
         //!@{
         //! Compute residual from right-hand side, subspace vectors, and reduced solution.
         template<typename FF, typename RHS, typename SUB, typename C> using has_compute_residual_type = decltype(std::declval<FF>().compute_residual(std::declval<RHS>(), std::declval<SUB>(), std::declval<C>()));
         template<template<typename...> typename VectorContainer, typename Coefs>
         constexpr bool has_compute_residual() const { return tulib::meta::is_detected_v<has_compute_residual_type, F, vec_t, VectorContainer<vec_t>, Coefs>; }

         //! Apply preconditioner to a vector
         template<typename FF, typename T> using has_precondition_type = decltype(std::declval<FF>().precondition(std::declval<T&>()));
         static constexpr bool has_precondition = tulib::meta::is_detected_v<has_precondition_type, F, vec_t>;

         //! Return true if the solver should flush and restart
         static constexpr bool has_restart = false; //< TODO: Implement

         //! Take RHS and return initial guess
         static constexpr bool has_initial_guess = false;   //< TODO: Implement
         //!@}

         //! Constructor from function and settings
         system_wrapper
            (  F&& f_
            )
            :  base(std::forward<F>(f_))
         {
         }

         //! Call matrix-vector transformation
         vec_t apply_matrix
            (  const vec_t& v_
            )
         {
            // TODO: Add timings here.

            ++this->_n_evals;
            return base::operator()(v_);
         }

         //! Get statistics
         size_t statistics
            (
            )  const noexcept
         {
            return this->_n_evals;
         }

      private:
         //! Number of function evaluations
         size_t _n_evals = 0;
   };

} /* namespace tulib::lin_solver::detail */

#endif /* TULIB_LIN_SOLVER_SYSTEM_WRAPPER_HPP_INCLUDED */
