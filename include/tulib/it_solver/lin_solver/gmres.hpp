#ifndef TULIB_LIN_SOLVER_GMRES_HPP_INCLUDED
#define TULIB_LIN_SOLVER_GMRES_HPP_INCLUDED

#include <tulib/it_solver/lin_solver/lin_solver_base.hpp>

namespace tulib::lin_solver
{

namespace detail
{
/**
 * Class representing the upper Hessenberg matrix.
 * It is stored as a vector of columns.
 **/
template
   <  typename T
   ,  template<typename...> typename C
   >
class hessenberg_matrix
   :  private C<std::vector<T>>
{
private:
   //! Current number of columns (outer vector is normally not cleared at restarts to avoid reallocation of columns)
   size_t _ncols = 0;

   //! For internal constexpr if statements
   static constexpr bool c_is_vector = std::is_same_v<C<std::vector<T>>, std::vector<std::vector<T>>>;
   static constexpr bool c_is_deque  = std::is_same_v<C<std::vector<T>>, std::deque<std::vector<T>>>;

   //! Function for internally getting the (i,j)'th element of the H matrix (disregarding the storage format)
   T& operator()(size_t i_, size_t j_)
   {
      // Pitfalls
      // - If first columns have been cut off, the column indices need to be taken into account. Use this->_ncols.
      // - If DQGMRES, we only store the non-zero values in each column. I.e. the row index must be shifted.
   }

public:
   //! c-tor from max dimension and DQGMRES truncation dim
   hessenberg_matrix
      (  size_t max_dim_
      )
   {
      // Reserve space for max_dim_ columns.
      if constexpr ( c_is_vector )
      {
         this->reserve(max_dim_);
      }
   }

   //! Pop the front column
   void pop_front()
   {
      if constexpr ( c_is_deque )
      {
         this->pop_front();
      }
      else
      {
         TULIB_EXCEPTION("Should only attempt to pop_front of H matrix when using deque.");
      }
   }

//   //! Perform backsubstitution
//   template
//      <  typename Vector
//      ,  template<typename...> typename C
//      >
//   void backsolve
//      (  Vector& x_
//      ,  const C<Vector>& subspace_
//      )
//   {
//      static_assert(false, "not impl");
//   }
//
//   //! Get element (perhaps not needed?)
//   T operator()(size_t row_, size_t col_) const
//   {
//      static_assert(false, "impl sanity checks");
//      return this->at(col_).at(row_);
//   }

   //! Apply Givens rotations
   void apply_givens_rotations
      (  const std::vector<T>& gamma_
      ,  const std::vector<T>& sin_
      ,  const std::vector<T>& cos_
      )

   //! Add column
   void emplace_column
      (  std::vector<T> c_
      )
   {
      if ( this->size() == this->_ncols )
      {
         this->push_back(std::move(c_));
      }
      else
      {
         this->operator[](this->_ncols) = std::move(c_);
      }
      ++this->_ncols;
   }

   //! Clear columns and set _ncols=0 (but preserve reserved memory)
   void clear()
   {
      for(auto& c : *this)
      {
         c.clear();
      }
      this->_ncols = 0;
   }

   //! Wipe all
   void wipe()
   {
      this->clear();
      this->_ncols = 0;
   }
};
} /* namespace detail */

/**
 * Linear-equation solver using the (preconditioned) GMRES algorithm
 **/
template
   <  typename F
   ,  template<typename...> typename SubspaceContainer
   >
class gmres_base
   :  public lin_solver_base<F>
{
public:
   //! Aliases from base class
   using base = lin_solver_base<F>;
   using typename base::param_type;
   using typename base::vec_type;
   using typename base::real_type;
   using typename base::rhs_type;
   using typename base::size_type;

   //! C-tor
   gmres_base
      (  F&& f_
      ,  const settings_type& settings_
      )
      :  base
         (  std::forward<F>(f_)
         ,  settings_
         )
      ,  _allow_restart(settings_.template get<bool>(inputKey::ALLOWRESTART))
   {
   }

private:
   //! H matrix (as vector of vectors to get Hessenberg structure)
   detail::hessenberg_matrix<param_type, SubspaceContainer> _h;

   //! Krylov subspace
   SubspaceContainer<vec_type> _subspace;

   //! Solution vector
   vec_type _x;

   //! Residual norm
   real_type _beta = std::numeric_limits<real_type>::max();

   //! Vector of residuals

   //! Prepare solver
   void prepare_solver_impl(const rhs_type& rhs_) override
   {
      // Initialize x0
      this->_x = this->initial_guess(rhs_);

      // Call reset_impl
      this->reset_impl(true);
   }

   //! Reset solver - allow start from scratch
   void reset_impl(bool first_) override
   {
      tulib::io::SCOPE_IOLEVEL(this->_writers, 3);
      tulib::io::SCOPE_INDENT(this->_writers);

      // If subspace is not empty (and !first_; don't they always come in pairs?), calculate the _x vector.
      static_assert(false);

      // Clear subspace
      this->_subspace.clear();

      // Clear sin, cos, gamma
      static_assert(false);

      // Compute r0 = b - Ax0
      auto r = this->_rhs->get();
      if ( !(first_ && this->_init_type != initType::ZERO) )
      {
         auto ax0 = this->apply_matrix(this->_x);
         tulib::math::wrap_axpy(r, param_type(-1.0), ax0);
      }

      // Precondition r
      this->precondition(this->_r);

      // Compute and save norm
      this->_beta = tulib::math::wrap_norm(r);

      // Save residual as first subspace vector and normalize it.
      this->_subspace.push_back(r);
      tulib::math::wrap_scale(this->_subspace.back(), real_type(1.)/this->_beta);

      this->_writers << "|beta|:   " << std::sqrt(this->_beta) << "\n"
                     << "#(A*v): " << this->statistics() << std::endl;
   }

   //! Clear
   void clear_impl() override
   {
      this->_subspace.clear();
      this->_h.wipe();
   }


   //! Run iteration
   bool run_iteration_impl(size_type iter_) override
   {
      tulib::io::SCOPE_IOLEVEL(this->_writers, 3);
      tulib::io::SCOPE_INDENT(this->_writers);

      // Do transformation (and preconditioning) of last subspace vector
      auto w = this->apply_matrix(this->_subspace.back());
      this->precondition(w);

      // Run modified Gram-Schmidt (orthogonalize last vector to all previous)
      std::vector<param_type> hcol;
      hcol.reserve(this->_subspace.size() + 1);
      for(const auto& v : this->_subspace)   // TODO: Implement iterative orthogonalization!
      {
         hcol.push_back(tulib::math::wrap_dot_product(w, v));
         tulib::math::wrap_axpy(w, -hcol.back(), v);
      }
      // Set norm of w vector as H(i+1, i), normalize w and add it to subspace.
      const auto wnorm = tulib::math::wrap_norm(w);
      hcol.push_back(wnorm);
      tulib::math::wrap_scale(w, real_type(1.)/wnorm);
      this->_h.emplace_column(std::move(hcol));
      this->_subspace.push_back(std::move(w));

      // Apply Givens rotations (and generate the last one)
      this->_h.apply_givens_rotations(this->_gamma, this->_sin, this->_cos);

      // Get error norm from gamma vector
      const auto rnorm = this->_gamma.back();

      // Check convergence
      const bool conv = this->check_convergence_impl(rnorm);

      // If DQGMRES, we update the _x vector here and truncate the subspace (and H matrix) if needed.
      // For restarted GMRES, only do it if converged.
      this->update_solution(conv);

      // Return
      return conv;
   }

   //! Get solution
   vec_type solution_impl() const override
   {
      // TODO: If restarted GMRES, check that the x vector has actually been computed!
      static_assert(false);
      return this->_x;
   }

   //! Update the solution vector
   virtual void update_solution(bool conv_) = 0;
};

/**
 * GMRES with restarts
 **/
template
   <  typename F
   >
class gmres
   :  public gmres_base<F, std::vector>
{
public:
   //! c-tor
   static_assert(false);

private:
   //! Maximum dimension of Krylov subspace. Restart after this number of iterations.
   size_type _restart_dim = 100;

   //! Type
   std::string_view type_impl() const override
   {
      return "GMRES";
   }

   //! Do we flush and restart?
   bool flush_and_restart_impl() const override
   {
      return this->_subspace.size() >= this->_restart_dim;
   }

   //! Update the solution vector (i.e. compute it)
   void update_solution(bool conv_) override
   {
      if ( conv_ )
      {
         static_assert(false, "not impl");
      }
   }
};

/**
 * DQGMRES
 **/
template
   <  typename F
   >
class dqgmres
   :  public gmres_base<F, std::deque>
{
public:
   //! c-tor
   static_assert(false);

private:
   //! Use DQGMRES with this number of vectors (0 means no truncation)
   size_type _dqgmres = 0;

   //! p vectors used for updating the solution
   std::deque<vec_type> _p;

   std::string_view type_impl() const override
   {
      return "DQGMRES(" + std::to_string(this->_subspace_dim).c_str() + ")";
   }

   //! DQGMRES does not use restarts
   bool flush_and_restart_impl() const override
   {
      return false;
   }

   //! Update the solution vector and truncate space
   void update_solution(bool conv_) override
   {
      static_assert(false, "not impl");
      // Do update
      // ...

      // Truncate subspace and H matrix if necessary
      // ...
   }
};

} /* namespace tulib::lin_solver */

#endif /* TULIB_LIN_SOLVER_GMRES_HPP_INCLUDED */