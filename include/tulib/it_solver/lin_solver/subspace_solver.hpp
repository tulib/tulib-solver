#ifndef TULIB_LIN_SOLVER_SUBSPACE_SOLVER_INCLUDED
#define TULIB_LIN_SOLVER_SUBSPACE_SOLVER_INCLUDED

namespace tulib::lin_solver
{

/**
 *
 **/
template
   <  typename F
   ,  template<typename...> typename VectorContainer
   >
class subspace_solver
   :  public lin_solver_base<F, VectorContainer>
{
public:
   //! Aliases
   template<typename T> using subspace_container_tmpl = std::deque<T>;
   using subspace_container = subspace_container_tmpl<vec_type>;
   using residuals_type = VectorContainer<vec_type>;

private:
   /** @name State (stored vectors, etc.) **/
   //!@{
   //! Subspace vectors
   subspace_container _subspace;

   //! Residuals (1 per RHS)
   VectorContainer<vec_t> _residuals;
   //!@}

   //! Clear
   void reset_impl() override
   {
      this->_subspace.clear();
      this->_residuals.clear();
   }

   //! Compute residuals
   template<typename Coefs, template<typename...> typename VectorContainer2>
   residuals_type compute_residuals
      (  const rhs_type& rhs_
      ,  const VectorContainer2<vec_type>& transformed_subspace_
      ,  const Coefs& coefs_
      )  const
   {
      if ( coefs_.size() != rhs_.size() )
      {
         TULIB_EXCEPTION("Size mismatch in number of solutions and RHSs.");
      }
      // Check if system_wrapper implements compute_residuals
      residuals_type res(rhs_.size());
      std::transform
#ifdef TULIB_PAR_STL
         (  std::execution::par_unseq
         ,  rhs_.begin(), rhs_.end()
#else
         (  rhs_.begin(), rhs_.end()
#endif
         ,  coefs_.begin()
         ,  res.begin()
         ,  [this, &transformed_subspace_, &coefs_](const vec_type& irhs_, const auto& c_)
            {
               return this->compute_residual(irhs_, transformed_subspace_, c_);
            }
         );

      return res;
   }

   //! Compute single residual
   template<typename Coefs, template<typename...> typename VectorContainer2>
   vec_type compute_residual
      (  const vec_type& rhs_
      ,  const VectorContainer2<vec_type>& transformed_subspace_
      ,  const Coefs& coefs_
      )  const
   {
      if ( coefs_.size() != transformed_subspace_.size() )
      {
         TULIB_EXCEPTION("Size mismatch in coefs and subspace.");
      }

      // Check if F provides an overload
      if constexpr ( this->has_compute_residual<VectorContainer2, Coefs>() )
      {
         return this->get().compute_residual(rhs_, transformed_subspace_, coefs_);
      }
      else
      {
         auto res = tulib::math::wrap_linear_combination(transformed_subspace_, coefs_);
         tulib::math::wrap_axpy(res, -1.0, rhs_);
         return res;
      }
   }

};

} /* tulib::lin_solver */

#endif /* TULIB_LIN_SOLVER_SUBSPACE_SOLVER_INCLUDED */