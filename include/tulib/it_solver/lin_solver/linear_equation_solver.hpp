#ifndef TULIB_LINEAR_EQUATION_SOLVER_INCLUDED
#define TULIB_LINEAR_EQUATION_SOLVER_INCLUDED

#include <vector>

#include <tulib/settings.hpp>
#include <tulib/it_solver/lin_solver/enums.hpp>
#include <tulib/it_solver/lin_solver/lin_solver_base.hpp>
#include <tulib/it_solver/lin_solver/factory.hpp>
#include <tulib/it_solver/lin_solver/defaults.hpp>

namespace tulib
{

namespace lin_solver::detail
{

//! Validate and return settings
settings_type get_validated_settings
   (  const settings_type& settings_
   )
{
   auto result = settings_;
   if ( result.validate(get_lin_solver_defaults()) )
   {
      return result;
   }
   else
   {
      TULIB_EXCEPTION("Invalid settings passed to linear_equation_solver.");
   }
}
settings_type get_validated_settings
   (  solverType type_
   ,  typename settings_type::float_type threshold_
   ,  typename settings_type::integer_type maxiter_
   )
{
   settings_type res;
   res.add(inputKey::SOLVER, std::make_any<solverType>(type_));
   res.add(inputKey::THRESHOLD, std::make_any<typename settings_type::float_type>(threshold_));
   res.add(inputKey::MAXITER, std::make_any<typename settings_type::integer_type>(maxiter_));
   return get_validated_settings(res);
}

} /* namespace lin_solver::detail */

/**
 * 
 **/
template
   <  typename F
   >
class linear_equation_solver
{
private:
   //! Private aliases
   using impl_type = lin_solver::lin_solver_base<F>;

public:
   //! Public aliases
   using real_type = typename impl_type::real_type;
   using size_type = typename impl_type::size_type;
   using vec_type = typename impl_type::vec_type;
   using rhs_type = typename impl_type::rhs_type;

public:
   //! Constructor from settings
   linear_equation_solver
      (  F&& f_
      ,  const lin_solver::settings_type& settings_
      )
      :  _settings
            (  lin_solver::detail::get_validated_settings(settings_)
            )
      ,  _impl(lin_solver::factory(std::forward<F>(f_), this->_settings))
   {
   }

   //! Constructor from type, threshold, and maxiter
   linear_equation_solver
      (  F&& f_
      ,  lin_solver::solverType type_
      ,  real_type threshold_
      ,  size_type maxiter_
      )
      :  _settings
            (  lin_solver::detail::get_validated_settings(type_, threshold_, maxiter_)
            )
      ,  _impl(lin_solver::factory(std::forward<F>(f_), this->_settings))
   {
   }


   //! Solve function
   std::pair<vec_type, bool> solve
      (  const rhs_type& rhs_
      ,  bool keep_internal_state_ = false
      )
   {
      // Prepare solver. 
      this->_impl->prepare_solver(rhs_);

      // Set up aux. variables
      bool converged = false;
      bool done = false;
      size_type iter = 0;

      // Run iterations
      while ( !(done || converged) )
      {
         std::tie(converged, done, iter) = this->_impl->run_solver(iter);

         if ( !done )
         {
            this->_impl->reset();
         }
      }

      // Write restart info
//      this->_impl->write_restart_info(iter);

      // Save statistics and clear internal state
//      this->save_statistics(converged, done, iter);
      if ( !keep_internal_state_ )
      {
         this->clear_internal_state();
      }

      // Return converged
      return std::make_pair(this->_impl->solution(), converged);
   }

   //! Clear internal state of solver
   void clear_internal_state()
   {
      this->_impl->clear();
   }

private:
   //! Settings
   lin_solver::settings_type _settings;

   //! Pointer to implementation
   std::unique_ptr<impl_type> _impl = nullptr;
};

//! Deduction guide
template<typename F> linear_equation_solver(F&&, const lin_solver::settings_type&) -> linear_equation_solver<F>;

} /* namespace tulib */

#endif /* TULIB_LINEAR_EQUATION_SOLVER_INCLUDED */