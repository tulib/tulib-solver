#ifndef TULIB_LIN_SOLVER_CONJUGATE_RESIDUAL_HPP_INCLUDED
#define TULIB_LIN_SOLVER_CONJUGATE_RESIDUAL_HPP_INCLUDED

#include <optional>

#include <tulib/it_solver/lin_solver/lin_solver_base.hpp>

namespace tulib::lin_solver
{

/**
 * Linear-equation solver using the (preconditioned) conjugate-gradient algorithm.
 **/
template
   <  typename F
   >
class conjugate_residual
   :  public lin_solver_base<F>
{
public:
   //! Aliases from base class
   using base = lin_solver_base<F>;
   using typename base::param_type;
   using typename base::vec_type;
   using typename base::real_type;
   using typename base::rhs_type;
   using typename base::size_type;

   //! C-tor
   conjugate_residual
      (  F&& f_
      ,  const settings_type& settings_
      )
      :  base
         (  std::forward<F>(f_)
         ,  settings_
         )
      ,  _allow_restart(settings_.template get<bool>(inputKey::ALLOWRESTART))
   {
   }

private:
   //! Enable restarting if the error increases
   bool _allow_restart = false;

   //! p: Direction vector
   vec_type _p;

   //! r: (preconditioned) residual vector
   vec_type _r;

   //! x: Solution vector
   vec_type _x;

   //! A*p vector
   vec_type _Ap;

   //! A*r vector
   vec_type _Ar;

   //! Squared residual norm
   real_type _rnorm2 = std::numeric_limits<real_type>::max();
   
   //! Previous squared residual norm
   real_type _prev_rnorm2 = std::numeric_limits<real_type>::max();

   //! Prepare solver (compute initial residual (r0) and set p0)
   void prepare_solver_impl(const rhs_type& rhs_) override
   {
      // Initialize x0
      this->_x = this->initial_guess(rhs_);

      // Call reset_impl
      this->reset_impl(true);
   }

   //! Reset solver - allow start from scratch
   void reset_impl(bool first_) override
   {
      // In the case of CR, we simply recompute the residual and restart the search directon.
      // The current best solution (_x) is kept.

      tulib::io::SCOPE_IOLEVEL(this->_writers, 3);
      tulib::io::SCOPE_INDENT(this->_writers);

      // Compute r0 = b - Ax0
      this->_r = this->_rhs->get();
      if ( !(first_ && this->_init_type != initType::ZERO) )
      {
         auto ax0 = this->apply_matrix(this->_x);
         tulib::math::wrap_axpy(this->_r, param_type(-1.0), ax0);
      }

      // If we are doing preconditioning
      if (  this->use_precon()
         )
      {
         this->precondition(this->_r);
      }

      // Save preconditioned |r|^2
      this->_rnorm2 = tulib::math::wrap_norm2(this->_r);
      this->_prev_rnorm2 = std::numeric_limits<real_type>::max();

      this->_writers << "|r0|:   " << std::sqrt(this->_rnorm2) << std::endl;

      // Set p0 = r0
      this->_p = this->_r;

      // Compute Ar
      this->_Ar = this->apply_matrix(this->_r);

      // Set Ap = Ar
      this->_Ap = this->_Ar;
   }

   //! Clear
   void clear_impl() override
   {
      // TODO: Should we remove all vectors (except for x and r perhaps)?
   }


   //! Run iteration
   bool run_iteration_impl(size_type iter_) override
   {
      tulib::io::SCOPE_IOLEVEL(this->_writers, 3);
      tulib::io::SCOPE_INDENT(this->_writers);

      // Compute <r_i|A|r_i>
      const auto rAr = tulib::math::wrap_dot_product(this->_r, this->_Ar);

      std::optional<vec_type> precon_Ap = std::nullopt;
      if ( this->use_precon() )
      {
         precon_Ap = this->_Ap;
         this->precondition(precon_Ap.value());
      }

      // Compute alpha_i = <r|A|r>/<Ap|M^-1|Ap>
      const auto alpha = rAr / (precon_Ap ? tulib::math::wrap_dot_product(this->_Ap, precon_Ap.value()) : tulib::math::wrap_norm2(this->_Ap));

      // Update x: x_{i+1} = x_i + alpha_i * p_i
      tulib::math::wrap_axpy(this->_x, alpha, this->_p);

      // Update r: r_{i+1} = r_i - alpha_i * M^-1 A p_i
      tulib::math::wrap_axpy(this->_r, -alpha, precon_Ap.value_or(this->_Ap));

      // Exit here if norm is sufficiently small.
      this->_prev_rnorm2 = this->_rnorm2;
      this->_rnorm2 = tulib::math::wrap_norm2(this->_r);
      this->_writers << (this->use_precon() ? "|M^-1*r|:    " : "|r|:    ") << std::sqrt(this->_rnorm2) << "\n"
                     << "#(A*v): " << this->statistics() << std::endl;
      if ( this->check_convergence_impl(this->_rnorm2) )
      {
         return true;
      }

      // Update Ar
      this->_Ar = this->apply_matrix(this->_r);

      // Compute beta_i = <r_{i+1}|A|r_{i+1}> / rAr
      const auto beta = tulib::math::wrap_dot_product(this->_r, this->_Ar) / rAr;

      // Update p: p_{j+1} = r_{j+1} + beta_j*p_j
      tulib::math::wrap_scale(this->_p, beta);
      tulib::math::wrap_axpy(this->_p, param_type(1.0), this->_r);

      // Update Ap: Ap_{j+1} = Ar_{j+1} + beta_j*Ap_j
      tulib::math::wrap_scale(this->_Ap, beta);
      tulib::math::wrap_axpy(this->_Ap, param_type(1.0), this->_Ar);

      // Not converged if we got here.
      return false;
   }

   //! Type
   std::string_view type_impl() const override
   {
      return "Conjugate-Residual";
   }

   //! Flush and restart (i.e. recalculate residual)
   bool flush_and_restart_impl() const override
   {
      return this->_allow_restart && (this->_prev_rnorm2 < this->_rnorm2);
   }

   //! Get solution
   vec_type solution_impl() const override
   {
      return this->_x;
   }
};

} /* namespace tulib::lin_solver */

#endif /* TULIB_LIN_SOLVER_CONJUGATE_RESIDUAL_HPP_INCLUDED */
