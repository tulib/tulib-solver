#ifndef TULIB_LIN_SOLVER_FACTORY_HPP_INCLUDED
#define TULIB_LIN_SOLVER_FACTORY_HPP_INCLUDED

#include <memory>

#include <tulib/it_solver/lin_solver/lin_solver_base.hpp>
#include <tulib/it_solver/lin_solver/conjugate_gradient.hpp>
#include <tulib/it_solver/lin_solver/conjugate_residual.hpp>
//#include <tulib/it_solver/lin_solver/gmres.hpp>

namespace tulib::lin_solver
{
   //! Factory method
   template<typename F>
   std::unique_ptr<lin_solver_base<F>> factory
      (  F&& f_
      ,  const lin_solver::settings_type& settings_
      )
   {
      // NB: Validate settings outside
      // ...

      const auto type = settings_.template get<solverType>(inputKey::SOLVER);
      switch ( type )
      {
         case solverType::CG:
            return std::make_unique<conjugate_gradient<F>>(std::forward<F>(f_), settings_);
         case solverType::CR:
            return std::make_unique<conjugate_residual<F>>(std::forward<F>(f_), settings_);
//         case solverType::GMRES:
//            return std::make_unique<gmres<F>>(std::forward<F>(f_), settings_);
//         case solverType::DQGMRES:
//            return std::make_unique<dqgmres<F>>(std::forward<F>(f_), settings_);
//         case solverType::BICGSTAB:
//            return std::make_unique<bicgstab<F>>(std::forward<F>(f_), settings_);
//         case solverType::TFQMR:
//            return std::make_unique<tfqmr<F>>(std::forward<F>(f_), settings_);
         default:
            TULIB_EXCEPTION("Unknown solver type.");
      }
   }


} /* namespace tulib::lin_solver */

#endif /* TULIB_LIN_SOLVER_FACTORY_HPP_INCLUDED */