#ifndef TULIB_LIN_SOLVER_DEFAULTS_HPP_INCLUDED
#define TULIB_LIN_SOLVER_DEFAULTS_HPP_INCLUDED

#include <tulib/util/input_definitions.hpp>
#include <tulib/util/io.hpp>
#include <tulib/it_solver/lin_solver/enums.hpp>

namespace tulib::lin_solver
{

/**
 * Get default settings
 **/
const auto& get_lin_solver_defaults
   (
   )
{
   using key_t = tulib::lin_solver::inputKey;
   using solver_t = tulib::lin_solver::solverType;
   const static typename util::input_definitions<key_t>::defaults_map_type<solver_t> def =
   {  {  key_t::SOLVER
      ,  {  {  solverType::CG    // Default for conjugate gradient
            ,  {  {key_t::PRECONDITIONER, std::make_any<preconType>(preconType::USER_DEFINED)}
               ,  {key_t::MAXITER, std::make_any<int>(100)}
               ,  {key_t::THRESHOLD, std::make_any<double>(1.e-6)}
               ,  {key_t::CONVCHECK, std::make_any<convType>(convType::RESIDUALNORM)}
               ,  {key_t::PRCG, std::make_any<bool>(false)}
               ,  {key_t::ALLOWRESTART, std::make_any<bool>(true)}
               ,  {key_t::OUTPUT, std::make_any<io::writer_collection>(io::writer_collection(std::cout, 5))}
               ,  {key_t::RESTARTPREFIX, std::make_any<std::string>("")}
               ,  {key_t::INITIALGUESS, std::make_any<initType>(initType::ZERO)}
               }
            }
         ,  {  solverType::CR    // Default for conjugate residual
            ,  {  {key_t::PRECONDITIONER, std::make_any<preconType>(preconType::USER_DEFINED)}
               ,  {key_t::MAXITER, std::make_any<int>(100)}
               ,  {key_t::THRESHOLD, std::make_any<double>(1.e-6)}
               ,  {key_t::CONVCHECK, std::make_any<convType>(convType::RESIDUALNORM)}
               ,  {key_t::ALLOWRESTART, std::make_any<bool>(true)}
               ,  {key_t::OUTPUT, std::make_any<io::writer_collection>(io::writer_collection(std::cout, 5))}
               ,  {key_t::RESTARTPREFIX, std::make_any<std::string>("")}
               ,  {key_t::INITIALGUESS, std::make_any<initType>(initType::ZERO)}
               }
            }
         }
      }
   };
   return def;
}

} /* namespace tulib::lin_solver */

#endif /* TULIB_LIN_SOLVER_DEFAULTS_HPP_INCLUDED */