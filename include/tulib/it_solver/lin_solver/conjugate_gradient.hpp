#ifndef TULIB_LIN_SOLVER_CONJUGATE_GRADIENT_HPP_INCLUDED
#define TULIB_LIN_SOLVER_CONJUGATE_GRADIENT_HPP_INCLUDED

#include <optional>

#include <tulib/it_solver/lin_solver/lin_solver_base.hpp>

namespace tulib::lin_solver
{

/**
 * Linear-equation solver using the (preconditioned) conjugate-gradient algorithm.
 **/
template
   <  typename F
   >
class conjugate_gradient
   :  public lin_solver_base<F>
{
public:
   //! Aliases from base class
   using base = lin_solver_base<F>;
   using typename base::param_type;
   using typename base::vec_type;
   using typename base::real_type;
   using typename base::rhs_type;
   using typename base::size_type;

   //! C-tor
   conjugate_gradient
      (  F&& f_
      ,  const settings_type& settings_
      )
      :  base
         (  std::forward<F>(f_)
         ,  settings_
         )
      ,  _prcg(settings_.template get<bool>(inputKey::PRCG))
      ,  _allow_restart(settings_.template get<bool>(inputKey::ALLOWRESTART))
   {
   }

private:
   //! Use Polak-Ribiere update formula instead of Fletcher-Reeves. Only makes sense if the preconditioner changes between iterations.
   bool _prcg = false;

   //! Enable restarting if the error increases
   bool _allow_restart = false;

   //! p: Direction vector
   vec_type _p;

   //! r: Residual vector
   vec_type _r;

   //! x: Solution vector
   vec_type _x;

   //! z: Preconditioned residual vector
   std::optional<vec_type> _z = std::nullopt;

   //! Squared residual norm
   real_type _rnorm2 = std::numeric_limits<real_type>::max();
   
   //! Previous squared residual norm
   real_type _prev_rnorm2 = std::numeric_limits<real_type>::max();

   //! Prepare solver (compute initial residual (r0) and set p0)
   void prepare_solver_impl(const rhs_type& rhs_) override
   {
      // Initialize x0
      this->_x = this->initial_guess(rhs_);

      // Call reset_impl
      this->reset_impl(true);
   }

   //! Reset solver - allow start from scratch
   void reset_impl(bool first_) override
   {
      // In the case of CG, we simply recompute the residual and restart the search directon.
      // The current best solution (_x) is kept.

      tulib::io::SCOPE_IOLEVEL(this->_writers, 3);
      tulib::io::SCOPE_INDENT(this->_writers);

      // Compute r0 = b - Ax0
      this->_r = this->_rhs->get();
      if ( !(first_ && this->_init_type != initType::ZERO) )
      {
         auto ax0 = this->apply_matrix(this->_x);
         tulib::math::wrap_axpy(this->_r, param_type(-1.0), ax0);
      }

      // Save |r|^2
      this->_rnorm2 = tulib::math::wrap_norm2(this->_r);
      this->_prev_rnorm2 = std::numeric_limits<real_type>::max();

      this->_writers << "|r0|:   " << std::sqrt(this->_rnorm2) << "\n"
                     << "#(A*v): " << this->statistics() << std::endl;

      // Set p0 = r0
      this->_p = this->_r;

      // If we are doing preconditioning (or using PR update), initialize z
      if (  this->use_precon()
         || this->_prcg
         )
      {
         this->_z = std::make_optional(this->_r);
         this->precondition(this->_z.value());
      }
   }

   //! Clear
   void clear_impl() override
   {
      // TODO: Should we remove x and r as well?

      this->_z = std::nullopt;
   }


   //! Run iteration
   bool run_iteration_impl(size_type iter_) override
   {
      tulib::io::SCOPE_IOLEVEL(this->_writers, 3);
      tulib::io::SCOPE_INDENT(this->_writers);

      // Transform: t_i = A*p_i
      const auto t = this->apply_matrix(this->_p);

      // Compute rnorm2 = ||r_i||^2 ( or <r_i|z_i> for preconditioned CG)
      const auto rzdot = this->_z ? tulib::math::wrap_dot_product(this->_r, this->_z.value()) : tulib::math::wrap_norm2(this->_r);

      // Compute alpha_i = rzdot / <p_i|t_i>
      const auto alpha = rzdot / tulib::math::wrap_dot_product(this->_p, t);

      // Update x: x_{i+1} = x_i + alpha_i * p_i
      tulib::math::wrap_axpy(this->_x, alpha, this->_p);

      // Update r: r_{i+1} = r_i - alpha_i * t_i
      tulib::math::wrap_axpy(this->_r, -alpha, t);

      // Exit here if norm is sufficiently small.
      this->_prev_rnorm2 = this->_rnorm2;
      this->_rnorm2 = tulib::math::wrap_norm2(this->_r);
      this->_writers << "|r|:    " << std::sqrt(this->_rnorm2) << std::endl;
      if ( this->check_convergence_impl(this->_rnorm2) )
      {
         return true;
      }

      // Compute <r_{i+1}|z_i> for PRCG if needed
      const auto prcg_correction = (this->_prcg && this->_z) ? tulib::math::wrap_dot_product(this->_r, this->_z.value()) : param_type(0);

      // Update z
      if ( this->_z )
      {
         this->_z = std::make_optional(this->_r);
         this->precondition(this->_z.value());
      }

      // Compute beta_i = <r_{i+1}|r_{i+1}> / rnorm2
      const auto beta = ((this->_z ? tulib::math::wrap_dot_product(this->_r, this->_z.value()) : this->_rnorm2) - prcg_correction) / rzdot;

      // Update p: p_{j+1} = r_{j+1} + beta_j*p_j
      tulib::math::wrap_scale(this->_p, beta);
      tulib::math::wrap_axpy(this->_p, param_type(1.0), this->_z.value_or(this->_r));

      // Not converged if we got here.
      return false;
   }

   //! Type
   std::string_view type_impl() const override
   {
      return "Conjugate-Gradient";
   }

   //! Flush and restart (i.e. recalculate residual)
   bool flush_and_restart_impl() const override
   {
      return this->_allow_restart && (this->_prev_rnorm2 < this->_rnorm2);
   }

   //! Get solution
   vec_type solution_impl() const override
   {
      return this->_x;
   }
};

} /* namespace tulib::lin_solver */

#endif /* TULIB_LIN_SOLVER_CONJUGATE_GRADIENT_HPP_INCLUDED */