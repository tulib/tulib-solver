#ifndef IT_SOLVER_NL_SOLVER_PICARD_HPP_INCLUDED
#define IT_SOLVER_NL_SOLVER_PICARD_HPP_INCLUDED

namespace tulib::nlsolver
{

/**
 * Picard (fixed-point iterator)
 **/
template
   <  typename F
   ,  bool FixedPoint = false
   >
class picard
   :  public detail::nl_solver_base<F, FixedPoint>
{
};

} /* namespace tulib::nlsolver */

#endif /* IT_SOLVER_NL_SOLVER_PICARD_HPP_INCLUDED */
