#ifndef IT_SOLVER_NL_SOLVER_ANDERSON_ACCELERATION_HPP_INCLUDED
#define IT_SOLVER_NL_SOLVER_ANDERSON_ACCELERATION_HPP_INCLUDED

namespace tulib::nlsolver
{

/**
 * Anderson acceleration (DIIS)
 **/
template
   <  typename F
   ,  bool FixedPoint = false
   >
class anderson_acceleration
   :  public nl_solver_base<F, FixedPoint>
{
};

} /* namespace tulib::nlsolver */

#endif /* IT_SOLVER_NL_SOLVER_ANDERSON_ACCELERATION_HPP_INCLUDED */
