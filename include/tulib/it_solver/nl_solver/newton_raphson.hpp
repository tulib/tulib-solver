#ifndef IT_SOLVER_NL_SOLVER_NEWTON_RAPHSON_HPP_INCLUDED
#define IT_SOLVER_NL_SOLVER_NEWTON_RAPHSON_HPP_INCLUDED

namespace tulib::nlsolver
{

/**
 * Newton-Raphson
 **/
template
   <  typename F
   >
class newton_raphson
   :  public detail::nl_solver_base<F, false>
{
};

} /* namespace tulib::nlsolver */

#endif /* IT_SOLVER_NL_SOLVER_NEWTON_RAPHSON_HPP_INCLUDED */
