#ifndef TULIB_NL_SYSTEM_WRAPPER_HPP_INCLUDED
#define TULIB_NL_SYSTEM_WRAPPER_HPP_INCLUDED

#include <tulib/util/function_wrapper.hpp>
#include <tulib/linalg/typedefs.hpp>
#include <tulib/util/exceptions.hpp>

namespace tulib::nlsolver
{
/**
 * Wrapper for system of non-linear equations
 **/
template
   <  typename F
   >
class nl_system_wrapper
   :  private tulib::util::function_wrapper<F>
{
   public:
      using func_t = tulib::it_solver::detail::function_container<F>;
      using vec_t = typename func_t::vec_t;
      using arg_t = typename func_t::arg_t;
      using value_t = decltype(tulib::math::wrap_dot_product(std::declval<vec_t>()));
      using real_t = tulib::meta::real_type_t<value_t>;
      using jacobian_t = tulib::internal_matrix_type<value_t>;

      //! Disable default c-tor
      nl_system_wrapper() = delete;

      //! Disable copy c-tor
      nl_system_wrapper(const nl_system_wrapper&) = delete;

      //! Disable copy assignment
      nl_system_wrapper& operator=(const nl_system_wrapper&) = delete;

      //! c-tor from rvalue ref
      nl_system_wrapper
         (  F&& f
         ,  real_t thresh = 1.e-12
         )
         :  func_t(std::forward<F>(f))
         ,  _threshold(thresh)
      {
      }

      //! c-tor from lvalue ref
      nl_system_wrapper
         (  F& f
         ,  real_t thresh = 1.e-12
         )
         :  func_t(std::forward<F>(f))
         ,  _threshold(thresh)
      {
      }

      //! Wrapper for function evaluation
      vec_t operator()
         (  const vec_t& v
         )
      {
         ++_feval;
         return this->f()(v);
      }

      //! Wrapper for precondition
      void WRAP_precondition
         (  vec_t& v
         )
      {
         if constexpr   (  enable_precondition()
                        )
         {
            this->f().precondition(v);
         }
      }

      //! Wrapper for convergence check
      bool WRAP_converged
         (  const vec_t& v
         )
      {
         if constexpr   (  enable_converged()
                        )
         {
            return this->f().converged(v);
         }
         else
         {
            return ( tulib::math::wrap_norm(v) < this->_threshold );
         }
      }

      //! Wrapper for jacobian matrix
      jacobian_t WRAP_jacobian
         (  const vec_t& v
         )
      {
         if constexpr   (  enable_jacobian()
                        )
         {
            return this->f().jacobian(v);
         }
         else
         {
            return this->numerical_jacobian(v);
         }
      }

      //! Wrapper for Jacobian transformation
      vec_t WRAP_jacobian_transform
         (  const vec_t& v
         )
      {
         if constexpr   (  enable_jacobian_transform()
                        )
         {
            return this->f().jacobian_transform(v);
         }
         else
         {
            return this->numerical_jacobian_transform(v);
         }
      }

      //! Wrapper for adjoint Jacobian transformation
      vec_t WRAP_adjoint_jacobian_transform
         (  const vec_t& v
         )
      {
         if constexpr   (  enable_adjoint_jacobian_transform()
                        )
         {
            return this->f().adjoint_jacobian_transform(v);
         }
         else
         {
            TULIB_EXCEPTION("Adjoint Jacobian transform is not implemented for this function type!");
         }
      }

      //! Get number of function evaluations
      size_t feval
         (
         )  const
      {
         return this->_feval;
      }

   private:
      //! Default convergence threshold (if check is not done by F)
      real_t _threshold = 1.e-12;

      //! Number of function evaluations
      size_t _feval = 0;

      /* @name aliases used for deducing if optional methods are implemented */
      //!@{
      template<typename FF, typename V> using has_precondition_type = decltype(std::declval<FF>().precondition(std::declval<V>()));
      template<typename FF, typename V> using has_converged_type = decltype(std::declval<FF>().converged(std::declval<V>()));
      template<typename FF, typename V> using has_jacobian_type = decltype(std::declval<FF>().jacobian(std::declval<V>()));
      template<typename FF, typename V> using has_jacobian_transform_type = decltype(std::declval<FF>().jacobian_transform(std::declval<V>()));
      template<typename FF, typename V> using has_adjoint_jacobian_transform_type = decltype(std::declval<FF>().adjoint_jacobian_transform(std::declval<V>()));
      //!@}

      //! Can we precondition?
      static constexpr bool enable_precondition
         (
         )
      {
         return tulib::meta::is_detected_v<has_precondition_type, F, vec_t>;
      }

      //! Do we have custom convergence check?
      static constexpr bool enable_converged
         (
         )
      {
         return tulib::meta::is_detected_v<has_converged_type, F, vec_t>;
      }

      //! Do we have a Jacobian?
      static constexpr bool enable_jacobian
         (
         )
      {
         return tulib::meta::is_detected_v<has_jacobian_type, F, vec_t>;
      }

      //! Do we have transformation with Jacobian matrix?
      static constexpr bool enable_jacobian_transform
         (
         )
      {
         return tulib::meta::is_detected_v<has_jacobian_transform_type, F, vec_t>;
      }

      //! Do we have transformation with adjoint Jacobian matrix?
      static constexpr bool enable_adjoint_jacobian_transform
         (
         )
      {
         return tulib::meta::is_detected_v<has_adjoint_jacobian_transform_type, F, vec_t>;
      }

      //! Calculate numerical Jacobian
      jacobian_t numerical_jacobian
         (  const vec_t& v
         )
      {
         TULIB_EXCEPTION("Not implemented!");
      }

      //! Calculate numerical Jacobian transform
      jacobian_t numerical_jacobian_transform
         (  const vec_t& v
         )
      {
         //! This can be done via a nice formula.
         // See e.g. the Saad book about linear equations under the BICGSTAB (or something)
         TULIB_EXCEPTION("Not implemented!");
      }
};

} /* namespace tulib::nlsolver */


#endif /* TULIB_NL_SYSTEM_WRAPPER_HPP_INCLUDED */
