#include <test/math_test.hpp>

namespace tulib::test
{

void math_test
   (
   )
{
   cutee::suite s("math_test_suite");

   // Add tests
   s.add_test<container_norm2_test<float>>("tulib::math::wrap_norm2 test (float)");
   s.add_test<container_norm2_test<double>>("tulib::math::wrap_norm2 test (double)");
   s.add_test<container_norm2_test<std::complex<float>>>("tulib::math::wrap_norm2 test (complex<float>)");
   s.add_test<container_norm2_test<std::complex<double>>>("tulib::math::wrap_norm2 test (complex<double>)");

   s.add_test<container_scale_test<float>>("tulib::math::wrap_scale test (float)");
   s.add_test<container_scale_test<double>>("tulib::math::wrap_scale test (double)");
   s.add_test<container_scale_test<std::complex<float>>>("tulib::math::wrap_scale test (complex<float>)");
   s.add_test<container_scale_test<std::complex<double>>>("tulib::math::wrap_scale test (complex<double>)");

   s.add_test<container_size_test<float>>("tulib::math::wrap_size test (float)");
   s.add_test<container_size_test<double>>("tulib::math::wrap_size test (double)");
   s.add_test<container_size_test<std::complex<float>>>("tulib::math::wrap_size test (complex<float>)");
   s.add_test<container_size_test<std::complex<double>>>("tulib::math::wrap_size test (complex<double>)");

   s.add_test<container_dot_product_test<float>>("tulib::math::wrap_dot_product test (float)");
   s.add_test<container_dot_product_test<double>>("tulib::math::wrap_dot_product test (double)");
   s.add_test<container_dot_product_test<std::complex<float>>>("tulib::math::wrap_dot_product test (complex<float>)");
   s.add_test<container_dot_product_test<std::complex<double>>>("tulib::math::wrap_dot_product test (complex<double>)");

   s.add_test<container_axpy_test<float>>("tulib::math::wrap_axpy test (float)");
   s.add_test<container_axpy_test<double>>("tulib::math::wrap_axpy test (double)");
   s.add_test<container_axpy_test<std::complex<float>>>("tulib::math::wrap_axpy test (complex<float>)");
   s.add_test<container_axpy_test<std::complex<double>>>("tulib::math::wrap_axpy test (complex<double>)");

   s.add_test<container_linear_combination_test<float>>("tulib::math::wrap_linear_combination test (float)");
   s.add_test<container_linear_combination_test<double>>("tulib::math::wrap_linear_combination test (double)");
   s.add_test<container_linear_combination_test<std::complex<float>>>("tulib::math::wrap_linear_combination test (complex<float>)");
   s.add_test<container_linear_combination_test<std::complex<double>>>("tulib::math::wrap_linear_combination test (complex<double>)");

   s.add_test<container_scaled_error_test<float>>("tulib::math::wrap_scaled_error test (float)");
   s.add_test<container_scaled_error_test<double>>("tulib::math::wrap_scaled_error test (double)");
   s.add_test<container_scaled_error_test<std::complex<float>>>("tulib::math::wrap_scaled_error test (complex<float>)");
   s.add_test<container_scaled_error_test<std::complex<double>>>("tulib::math::wrap_scaled_error test (complex<double>)");
   
   run_suite(s);
}

} /* namespace tulib::test */
