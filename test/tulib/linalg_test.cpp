#include <cutee/cutee.hpp>
#include <test/linalg_test.hpp>

namespace tulib::test
{

void linalg_test
   (
   )
{
   cutee::suite s("linalg_test_suite");

   // Add tests
   s.add_test<blas_test<float>>("BLAS test (float)");
   s.add_test<blas_test<double>>("BLAS test (double)");
   s.add_test<blas_test<std::complex<float>>>("BLAS test (complex<float>)");
   s.add_test<blas_test<std::complex<double>>>("BLAS test (complex<double>)");

   s.add_test<linear_system_test<float>>("Linear-system test (float)");
   s.add_test<linear_system_test<double>>("Linear-system test (double)");
   s.add_test<linear_system_test<std::complex<float>>>("Linear-system test (complex<float>)");
   s.add_test<linear_system_test<std::complex<double>>>("Linear-system test (complex<double>)");
   
   run_suite(s);
}

} /* namespace tulib::test */
