#include <test/lin_solver_test.hpp>

#include <complex>

namespace tulib::test
{

void lin_solver_test
   (
   )
{
   cutee::suite s("lin_solver_test_suite");

   // CG
   s.add_test<conjugate_gradient_test<float>>("conjugate-gradient test (float)");
   s.add_test<conjugate_gradient_test<double>>("conjugate-gradient test (double)");
   s.add_test<conjugate_gradient_test<std::complex<float>>>("conjugate-gradient test (complex float)");
   s.add_test<conjugate_gradient_test<std::complex<float>>>("conjugate-gradient test (complex double)");
   s.add_test<diag_precon_conjugate_gradient_test<float>>("diagonally-preconditioned conjugate-gradient test (float)");
   s.add_test<diag_precon_conjugate_gradient_test<double>>("diagonally-preconditioned conjugate-gradient test (double)");
   s.add_test<diag_precon_conjugate_gradient_test<std::complex<float>>>("diagonally-preconditioned conjugate-gradient test (complex float)");
   s.add_test<diag_precon_conjugate_gradient_test<std::complex<double>>>("diagonally-preconditioned conjugate-gradient test (complex double)");
//   s.add_test<pr_conjugate_gradient_test>("conjugate-gradient test using Polak-Ribiere update formula");
//   s.add_test<idja_precon_conjugate_gradient_test>("conjugate-gradient test using Polak-Ribiere update formula and IDJA preconditioner");
//   s.add_test<lbfgs_precon_conjugate_gradient_test>("conjugate-gradient test using Polak-Ribiere update formula and L-BFGS preconditioner");

   // CR
   s.add_test<conjugate_residual_test<float>>("conjugate-residual test (float)");
   s.add_test<conjugate_residual_test<double>>("conjugate-residual test (double)");
   s.add_test<conjugate_residual_test<std::complex<float>>>("conjugate-residual test (complex float)");
   s.add_test<conjugate_residual_test<std::complex<float>>>("conjugate-residual test (complex double)");
   s.add_test<diag_precon_conjugate_residual_test<float>>("diagonally-preconditioned conjugate-residual test (float)");
   s.add_test<diag_precon_conjugate_residual_test<double>>("diagonally-preconditioned conjugate-residual test (double)");
   s.add_test<diag_precon_conjugate_residual_test<std::complex<float>>>("diagonally-preconditioned conjugate-gradient test (complex float)");
   s.add_test<diag_precon_conjugate_residual_test<std::complex<double>>>("diagonally-preconditioned conjugate-gradient test (complex double)");
//   s.add_test<pr_conjugate_gradient_test>("conjugate-gradient test using Polak-Ribiere update formula");
//   s.add_test<idja_precon_conjugate_gradient_test>("conjugate-gradient test using Polak-Ribiere update formula and IDJA preconditioner");
//   s.add_test<lbfgs_precon_conjugate_gradient_test>("conjugate-gradient test using Polak-Ribiere update formula and L-BFGS preconditioner");
   
   run_suite(s);
}

} /*namespace tulib::test */
