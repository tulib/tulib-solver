#ifndef TULIB_TEST_DRIVER_WRAPPER_HPP_INCLUDED
#define TULIB_TEST_DRIVER_WRAPPER_HPP_INCLUDED

#include <map>
#include <string>
#include <functional>
#include <vector>

namespace tulib::test
{

/**
 * object for holding all test drivers
 **/
class driver_wrapper
{
   public:
      using key_t = std::string;
      using driver_t = std::function<void()>;

      //! c-tor
      driver_wrapper
         (
         )  = default;

      //! Get drivers
      const std::vector<driver_t> get_drivers
         (
         )  const;

      //! Select driver
      void select_driver
         (  const key_t&
         );

   private:
      //! Selected drivers
      std::vector<key_t> _selected_drivers;

      //! Available drivers
      const std::map<key_t, driver_t>& available_drivers
         (
         )  const;

      //! Check if key is valid
      bool is_valid_driver
         (  const key_t&
         )  const;

      //! Parse key_t
      key_t parse
         (  const key_t&
         )  const;
};

} /* namespace tulib::test */

#endif /* TULIB_TEST_DRIVER_WRAPPER_HPP_INCLUDED */
